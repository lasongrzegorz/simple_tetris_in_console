import os
import random

from copy import deepcopy

# Global constants
BOARD_SIZE = 20
PLAYING_AREA = BOARD_SIZE - 2  # board size excluding side walls

FULL_COORDINATES = 1
EMPTY_COORDINATES = 0
MOVE_LEFT = "a"
MOVE_RIGHT = "d"
# coordinates aliases for easier reading
X = 1
Y = 0

SHAPES = {
    "line": [[1, 1, 1, 1]],
    "l-shape": [[1, 0], [1, 0], [1, 1]],
    "l-shape-rev": [[0, 1], [0, 1], [1, 1]],
    "kicked-square": [[0, 1], [1, 1], [1, 0]],
    "square": [[1, 1], [1, 1]],
}


def create_board() -> list:
    """Create board including walls"""

    board = []
    right_wall = BOARD_SIZE - 1
    left_wall = 0
    bottom_wall = BOARD_SIZE - 1

    for i in range(BOARD_SIZE):
        board.append([])
        for j in range(BOARD_SIZE):
            if i == bottom_wall or j == left_wall or j == right_wall:
                board[i].append(FULL_COORDINATES)
            else:
                board[i].append(EMPTY_COORDINATES)

    return board


def print_board(board: list):
    """Print board to terminal clearing out screen beforehand"""

    # clear console before each print in both Win and Linux
    os.system("cls" if os.name == "nt" else "clear")

    for i in range(BOARD_SIZE):
        for j in range(BOARD_SIZE):
            if board[i][j] == FULL_COORDINATES:
                print("*", end="")
            else:
                print(" ", end="")
        print("")

    print("Instructions:\n")
    print("Press 'a' to move a shape left")
    print("Press 'd' to move a shape right")


def get_next_random_shape() -> list:
    """Chose a random shape from SHAPES global dictionary"""

    return random.choice(list(SHAPES.values()))


def get_shape_starting_coordinates() -> list:
    """Select random point a top line and return its coordinates"""

    y_coordinate = 0
    x_coordinate = random.randrange(1, PLAYING_AREA)

    return [y_coordinate, x_coordinate]


def update_shape_on_board(board: list, shape_coordinates: list, shape: list) -> list:
    """Using origin board, create new one with shape placed accordingly"""
    board_copy = deepcopy(board)
    # treat shape as quadrangle
    shape_size_y = len(shape)
    shape_size_x = len(shape[0])

    for i in range(shape_size_y):
        for j in range(shape_size_x):
            board_copy[shape_coordinates[Y] + i][shape_coordinates[X] + j] = shape[i][j]

    return board_copy


def move_shape_left(shape_coordinates: list) -> list:
    """Move shape one to the left"""
    next_coordinates = [shape_coordinates[Y], shape_coordinates[X] - 1]
    return next_coordinates


def move_shape_right(shape_coordinates: list) -> list:
    """Move shape one to the right"""

    return [shape_coordinates[Y], shape_coordinates[X] + 1]


def move_shape_down(shape_coordinates: list) -> list:
    """Move shape one line down"""

    return [shape_coordinates[Y] + 1, shape_coordinates[X]]


def is_move_possible(board, shape, shape_coordinates, move_direction):
    """Check if coming move does not go outside the playing area"""

    # check size of the shape first
    shape_width = len(shape[0])
    shape_height = len(shape)

    # get shape expected coordinates
    if move_direction == MOVE_RIGHT:
        shape_coordinates = move_shape_right(shape_coordinates)
    else:
        shape_coordinates = move_shape_left(shape_coordinates)

    for i in range(shape_height):
        for j in range(shape_width):
            if board[shape_coordinates[Y] + i][shape_coordinates[X] + j] == 1:
                return False
    return True


if __name__ == "__main__":
    running = True

    board = create_board()
    shape = get_next_random_shape()
    shape_coordinates = get_shape_starting_coordinates()
    initial_board = update_shape_on_board(board, shape_coordinates, shape)
    print_board(initial_board)

    user_move = input("Your move: ")

    while running:
        if user_move in [MOVE_LEFT, MOVE_RIGHT]:
            if is_move_possible(board, shape, shape_coordinates, user_move):
                if user_move == MOVE_LEFT:
                    shape_coordinates = move_shape_left(shape_coordinates)
                else:
                    shape_coordinates = move_shape_right(shape_coordinates)
            else:
                print(f"***\nMove is not possible!!!\n***")

            shape_coordinates = move_shape_down(shape_coordinates)
            board_after_move = update_shape_on_board(board, shape_coordinates, shape)
            print_board(board_after_move)
            user_move = input("Your move: ")
        else:
            user_move = input(
                "***Wrong move***\n" "Check instructions and move again: "
            )
